 An exercise in TypeScript, React and Ramda using Contentful as a CMS. The goal was to create a list of words based on book quotes then use a word to find the associated quotes.
