import Store from '../redux/store/configureStore';
import {ContentfulEntries} from "./apiITypes";
import {loadDataByEntryType} from "../redux/actions/entries/entriesActions";
import {addWordLists} from "../redux/actions/words/wordActions";
import {setStatus} from "../redux/actions/api/apiActions";

const contentful = require('contentful');


const client = contentful.createClient({
    space: '',
    accessToken: ''

})

export const loadContentfulEntries = () => {
    Store.dispatch(setStatus('loading'))
    client.getEntries().then(
        (entries: ContentfulEntries) => {
            Store.dispatch(loadDataByEntryType(entries))
            Store.dispatch(addWordLists())
            Store.dispatch(setStatus('loaded'))
        }
    ).catch(
        Store.dispatch(setStatus('error'))
    )
}






