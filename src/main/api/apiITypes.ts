

export interface ContentfulEntry {
    readonly sys: {
        readonly id: String
        readonly contentType: {
            readonly sys: {
                readonly id: String
            }
        }
    }
    readonly fields: {
        readonly quote? : String,
        readonly title? : String,
        readonly description? : String,
        readonly name? : String,
        readonly bio?: String
        readonly book?: {
            readonly id: String,
            readonly fields: {
                readonly name: String,
                readonly bio: String
            }
        },
        readonly author?: {
            readonly id: String,
            readonly fields: {
                readonly name: String,
                readonly: String
            }

        }
    }
};

export interface ContentfulEntries {
    items: Array<Object>
};






