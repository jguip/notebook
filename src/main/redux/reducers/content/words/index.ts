import ActionTypes from "../../../actions/actionTypes";
import State from "./State";
import {AddAllWords, AddWordsByQuote, AddFilteredWords, SetCurrentWord} from "../../../actions/words/wordActionTypes";

const InitialState: State = {
    wordsByQuote: [],
    allWords: [],
    filteredWords: [],
    currentWord: ''
}


export default function words(state: State = InitialState, action: ActionTypes) {
    switch (action.type) {
        case 'ADD_WORDS_BY_QUOTE':
            return {
                ...state,
                wordsByQuote: onAddWordsByQuote(state, action)
            }
        case 'ADD_ALL_WORDS':
            return {
                ...state,
                allWords: onAddAllWords(state, action)
            }
        case 'ADD_FILTERED_WORDS' :
            return {
                ...state,
                filteredWords: onAddFilteredWords(state, action)
            }
        case 'SET_CURRENT_WORD' :
            console.log(action)
            return {
                ...state,
                currentWord: onSetCurrentWord(state, action)

            }
        default:
            return state
    }

}

const onSetCurrentWord: (state: State, action: SetCurrentWord) => String = (state, action) => {
    return action.payload.data
}


const onAddWordsByQuote:  (state: State, action: AddWordsByQuote) => Array<Object> = (state, action) => {
    return state.wordsByQuote.concat(action.payload.data)
}

const onAddAllWords: (state: State, action: AddAllWords) => Array<Object> = (state, action) => {
    return state.allWords.concat(action.payload.data)
}

const onAddFilteredWords: (state: State, action: AddFilteredWords) => Array<Object> = (state, action) => {
    return state.filteredWords.concat(action.payload.data)
}



