
type State = {
    wordsByQuote: Array<Object>
    allWords: Array<Object>
    filteredWords: Array<Object>
    currentWord: String
}


export default State
