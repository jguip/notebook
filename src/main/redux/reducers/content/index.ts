import { combineReducers } from "redux";
import State from "../../store/State";
import entries from './entries'
import words from  './words'


const content = combineReducers<State>({
    entries,
    words
});

export default content;