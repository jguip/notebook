
type State = {
    entries: Array<Object>,
    quotes: Array<Object>,
    authors: Array<Object>,
    books: Array<Object>,
    currentQuotes: Array<Object>

}

export default State


