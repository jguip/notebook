import ActionTypes from "../../../actions/actionTypes";
import State from "./State";

import {
    AddBookAction,
    AddQuoteAction,
    LoadAllEntriesAction,
    AddAuthorAction,
    UpdateCurrentQuotesAction
} from "../../../actions/entries/entriesActionTypes";

const InitialState: State = {
    entries: [],
    quotes: [],
    authors: [],
    books: [],
    currentQuotes: []
}


export default function entries(state: State = InitialState, action: ActionTypes) {
    switch (action.type) {
        case 'LOAD_ALL_ENTRIES':
            return onLoadAllEntries(state, action)
        case 'ADD_QUOTE' :
            return onAddQuote(state, action)
        case 'ADD_BOOK' :
            return onAddBook(state, action)
        case 'ADD_AUTHOR' :
            return onAddAuthor(state, action)
        case 'UPDATE_CURRENT_QUOTES' :
            return onUpdateCurrentQuotes(state, action)
        default:
            return state
    }

}


const onUpdateCurrentQuotes = (state: State, action: UpdateCurrentQuotesAction) => {
    return {
        ...state,
        currentQuotes: action.payload.data
    }
}

const onLoadAllEntries = (state: State, action: LoadAllEntriesAction): Object => {
    return {
        ...state,
        entries: state.entries.concat(action.payload.data)
    }
}


const onAddQuote = (state: State, action: AddQuoteAction): Object => {
    return {
        ...state,
        quotes: state.quotes.concat(action.payload.data)
    }

}

const onAddBook = (state: State, action: AddBookAction): Object => {
    return{
        ...state,
        books: state.books.concat(action.payload.data)
    }

}

const onAddAuthor = (state: State, action: AddAuthorAction): Object => {
    return{
        ...state,
        authors: state.authors.concat(action.payload.data)
    }

}
