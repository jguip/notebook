import { combineReducers } from "redux"
import State from "../store/State"
import content from './content'
import app from './app'




const rootReducer = combineReducers<State>({
    content,
    app
});

export default rootReducer;