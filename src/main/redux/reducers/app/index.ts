import { combineReducers } from "redux";
import State from "../../store/State";
import api from './api/'


const app = combineReducers<State>({
    api
});

export default app;