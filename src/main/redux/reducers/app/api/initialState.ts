
type InitialState = {
    loadingStatus: String
}

const InitialState: InitialState = {
    loadingStatus: 'default'
}

export default InitialState
