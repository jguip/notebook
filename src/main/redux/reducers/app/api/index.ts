import ApiActionTypes from "../../../actions/apiActionTypes";
import InitialState from "./initialState";


export default function api(state: InitialState, action: ApiActionTypes) {
    switch (action.type) {
        case 'SET_API_STATUS':
            return onSetApiStatus(state, action)

        default:
            return InitialState
    }

}

const onSetApiStatus = (state: InitialState, action: ApiActionTypes) => {
    return{
        ...state,
        loadingStatus: action.payload.status
    }
}

