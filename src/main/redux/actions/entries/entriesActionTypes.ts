import keys from "../actionTypeKeys";

export interface LoadAllEntriesAction {
    readonly type: keys.LOAD_ALL_ENTRIES;
    readonly payload: {
        data: Object
    }
}

export interface AddQuoteAction {
    readonly type: keys.ADD_QUOTE;
    readonly payload: {
        data: Object;
    };
}

export interface AddBookAction {
    readonly type: keys.ADD_BOOK;
    readonly payload: {
        readonly data: Object;
    };
}

export interface AddAuthorAction {
    readonly type: keys.ADD_AUTHOR;
    readonly payload: {
        readonly data: Object;
    };
}


export interface UpdateCurrentQuotesAction {
    readonly type: keys.UPDATE_CURRENT_QUOTES;
    readonly payload: {
        readonly data: undefined
    }
}

export interface AddHighlightToCurrentQuotes {
    readonly type: keys.ADD_HIGHLIGHTS_TO_CURRENT_QUOTES
}