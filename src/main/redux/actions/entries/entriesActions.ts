import {Action, ActionCreator, Dispatch} from 'redux'
import {reduce, map} from 'ramda'
import {ThunkAction} from 'redux-thunk'
import keys from '../actionTypeKeys'
import State from '../../store/State'
import {Quote} from '../../store/stateTypes'
import {ContentfulEntry, ContentfulEntries} from "../../../api/apiITypes";


export const loadDataByEntryType: ActionCreator<ThunkAction<Action, State, void>> = (entry: ContentfulEntries) => {
    return (dispatch: Dispatch<State>): any => {
        const items: Array<Object> = entry.items
        items.forEach(
            (entry: ContentfulEntry) => {
                const entryType: String = entry.sys.contentType.sys.id
                switch (entryType) {
                    case 'quote': {
                        dispatch(loadQuoteData(entry))
                        return false
                    }
                    case 'book': {
                        dispatch(loadBookData(entry))
                        return false
                    }
                    case 'author': {
                        dispatch(loadAuthorData(entry))
                        return false
                    }
                    default :
                        return false
                }
            }
        )
        return false

    }
}

///quote

const loadQuoteData: ActionCreator<ThunkAction<Action, State, void>> = (entry: ContentfulEntry) => {
    return (dispatch: Dispatch<State>): Action => {
        return dispatch({
            payload: {data: formatQuoteData(entry)},
            type: keys.ADD_QUOTE,
        })
    }
}

export const getAndUpdateCurrentQuotes: ActionCreator<ThunkAction<void, State, void>> = (quoteIds: Array<String>) => {

    return (dispatch: Dispatch<State>, getState: () => State) => {

        const {content} = getState()
        const currentWord: String = content.words.currentWord
        const selectedQuotes = getSelectedQuotes(content.entries.quotes, quoteIds)
        const highlightedQuotes = highlightWordInQuotes(selectedQuotes, currentWord)

        dispatch(updateCurrentQuotes(highlightedQuotes))

    }
}


export const getSelectedQuotes: (quotes: Array<Object>, quoteIds: Array<String>) => Array<Object> =
    (quotes, quoteIds) => {
        return reduce(
        (acc: Array<Object>, quote: Quote) => {
            quoteIds.forEach(
                id => {
                    if (id === quote.id) {
                        acc.push(quote)

                    }
                }
            )
            return acc
        },
        [],
        quotes

    )
}


export const highlightWordInQuotes:  (quotes: Array<Object>, currentWord: String) =>  Array<Object> =
    (quotes, currentWord) => {
        return map((quote: Quote) => highlightWordInQuote(quote, currentWord), quotes)
    }



export const highlightWordInQuote: (quote: Quote, currentWord: String) =>  Object = (quote, currentWord ) => {

    const wordArray: Array<String> = quote.body.split(' ')

    const wordArrayWithHTML =
        map(word => {
            if(word === currentWord){
                return `<span class="wordHighlight">${word}</span>`
            }else{
                return word
            }
            }, wordArray)



    quote.body =  wordArrayWithHTML.join(' ')
    console.log(quote)

    return quote
}




const updateCurrentQuotes: ActionCreator<Action> = (data: Array<Object>) => {
    return {
        type: keys.UPDATE_CURRENT_QUOTES,
        payload: {
            data: data
        }
    }
}


const formatQuoteData = (data: ContentfulEntry): Object => {
    const author: Object = buildQuoteAuthorObject(data.fields.author)
    const book: Object = buildQuoteBookObject(data.fields.book)
    const body: String | undefined = data.fields.quote
    const id: String = data.sys.id
    return Object.assign({body, id, author, book}, {})

}


const buildQuoteBookObject = (book: any): Object => {
    return book !== undefined ?
        {
            bookId: book.sys.id,
            bookTitle: book.fields.title,
            bookDescription: book.fields.description

        } : {}
}


const buildQuoteAuthorObject = (author: any): Object => {
    return author !== undefined ?
        {
            authorName: author.fields.name,
            authorBio: author.fields.bio,
            authorId: author.sys.id,
        } : {}

}


//book

const loadBookData: ActionCreator<ThunkAction<Action, State, void>> = (entry: ContentfulEntry) => {
    return (dispatch: Dispatch<State>): Action => {
        return dispatch({
            payload: {data: formatBookData(entry)},
            type: keys.ADD_BOOK,

        })
    }
}


const formatBookData = (data: ContentfulEntry): Object => {
    return {
        bookTitle: data.fields.title,
        bookDescription: data.fields.description,
        id: data.sys.id
    }

}


//author


const loadAuthorData: ActionCreator<ThunkAction<Action, State, void>> = (entry: ContentfulEntry) => {
    return (dispatch: Dispatch<State>): Action => {
        return dispatch({
            payload: {data: formatAuthorData(entry)},
            type: keys.ADD_AUTHOR,
        })
    }
}

const formatAuthorData = (data: ContentfulEntry): Object => {
    return {
        authorName: data.fields.name,
        authorBio: data.fields.bio,
        id: data.sys.id
    }

}

