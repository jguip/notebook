import keys from "../actionTypeKeys";
import {SetApiStatus} from "./apiActionTypes";
import {Action, ActionCreator, Dispatch} from 'redux';
import {ThunkAction} from 'redux-thunk';
import State from '../../store/State'


export const setStatus : ActionCreator<ThunkAction<Action, State, void >> = (status: String) => {
    return (dispatch: Dispatch<State>) : SetApiStatus => {
        return dispatch(setStatusAction(status))
    }
}

export function setStatusAction(status: String) : SetApiStatus {
    return{
        type: keys.SET_API_STATUS,
        payload:{status: status}
    }

}

