import keys from "../actionTypeKeys";

export interface SetApiStatusLoading {
    readonly type: keys.SET_API_STATUS_LOADING;
    readonly payload: {
        status: String
    }
}

export interface SetApiStatusLoaded {
    readonly type: keys.SET_API_STATUS_LOADED;
    readonly payload: {
        status: String
    }
}

export interface SetApiStatusError {
    readonly type: keys.SET_API_STATUS_ERROR;
    readonly payload: {
        status: String
    }
}

export interface SetApiStatus {
    readonly type: keys.SET_API_STATUS;
    readonly payload: {
        status: String
    }
}
