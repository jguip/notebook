import {
    LoadAllEntriesAction,
    AddQuoteAction,
    AddBookAction,
    AddAuthorAction,
    UpdateCurrentQuotesAction
} from "./entries/entriesActionTypes";

import {
    SetApiStatusLoading,
    SetApiStatusLoaded,
    SetApiStatusError
} from "./api/apiActionTypes";

import {
    AddWordsByQuote,
    AddAllWords,
    AddFilteredWords,
    SetCurrentWord
} from "./words/wordActionTypes";

type ActionTypes =
    LoadAllEntriesAction|
    AddQuoteAction|
    AddBookAction|
    AddAuthorAction |
    AddWordsByQuote |
    AddFilteredWords |
    AddAllWords |
    UpdateCurrentQuotesAction |
    SetApiStatusLoading |
    SetApiStatusLoaded |
    SetApiStatusError |
    SetCurrentWord


export default ActionTypes;