import * as R from 'ramda'
import {shuffle} from 'lodash'
import {Dispatch, ActionCreator, Action} from "redux"
import {List} from 'immutable'
import State from "../../store/State"
import {Quote} from "../../store/stateTypes"
import {ThunkAction} from "redux-thunk"
import keys from "../actionTypeKeys"


const exclusions: List<String> = List([
    'Is', 'is', 'of', 'for', 'you', 'to', 'A', 'a', 'Or', 'or', 'The', 'the', 'in', 'at', 'they', 'will', 'no',
    'It', 'it', 'no', 'And', 'and', 'that', 'this', 'all', 'there', 'But', 'but', 'They',
    'up', 'was', 'Was', 'when', 'if', 'wanted', 'on', 'On', 'from'
])

export interface Word{
    word: String,
    quoteIds: Array<String>
}

export const addWordLists: ActionCreator<ThunkAction<void, State, void>> = () => {

    return (dispatch: Dispatch<State>, getState: () => State) => {

        const {content} = getState()
        const wordsByQuote: Array<Object>  =  buildQuoteWordList(content.entries.quotes)

        dispatch(addWordsByQuote(wordsByQuote));
        dispatch(addAllWords(buildFlatWordList(wordsByQuote)))

        //TODO: COMPOSE THIS
        dispatch(addFilteredWords(
            randomizeWordList(
            mergeDuplicateWords(
                removeExcludedWords(
                    removeEmptyWords(
                        removeAllPunctuation(
                            buildFlatWordList(wordsByQuote))))))))

    }
}

const mergeDuplicateWords: (words: Array<Object>) => Array<Object> = (words) => {

    return  R.reduce(
        (acc: Array<Object>, word: Word) => {
            if (acc.length === 0 || R.find(R.propEq('word', word.word))(acc) === undefined){
                acc.push(word)
            }else{
                acc =  acc.map(
                    (entry:Word) => {
                        if(entry.word === word.word && !R.contains(entry.quoteIds[0], word.quoteIds)){
                            entry.quoteIds.push(word.quoteIds[0])
                            return entry
                        }else{
                            return entry
                        }
                    }
                )
            }
            return acc
        },
        [],
        words
    )
}


const randomizeWordList: (words: Array<Object>) => Array<Object> = (words) => shuffle(words)


const buildQuoteWordList: (quotes: Array<Object> ) => Array<Object> = quotes =>
    quotes.map((quote: Quote) => groupWordsByQuote(quote.body, quote.id))


const groupWordsByQuote: (body: String, id: String) => Object = function(body: String, id: String){
    const quoteWords = body.split(' ')
    return R.reduce(
        (acc: Array<Object>, word: string) => {
                if(!isNumeric(word)){
                    acc.push(buildWord(convertWordCase(word), id))
                }
                return acc
        },
        [],
    quoteWords
    )

}

const convertWordCase: (word: String) => String = (word) => word.toLowerCase()

const buildWord: (word: String, id: String) =>  Object = function (word: String, id: String){
    return {
        word: word,
        quoteIds: [id],
    }
}

const isNumeric: (word: string) =>  Boolean  = (word) => {
    return /\d/.test(word);
}

const buildFlatWordList: (words: Array<Object>) => Array<Object> = words => R.flatten(words)

const removeAllPunctuation: (words: Array<Object>) => Array<Object> = (words) => {
    const wordLens = R.lensProp('word')
    const newWordList = words.map((word: Word) => R.set(wordLens, removePunctuation(word.word), word))
    return newWordList
}

const removeEmptyWords: (words: Array<Object>) => Array<Object> = (words) => {
    const newWordList =  words.filter((word: Word) => word.word !== '')
    return newWordList
}


const removePunctuation: (word:String) => String = (word) => {
    const noPunctuation = word.replace(/[\.,\/#!$%\^&\*;:{}=\_`~()@\+\?\"><\[\]\+]/g, '')
    const finalString =   noPunctuation.replace(/\s{2,}/g," ");
    return finalString
}

const removeExcludedWords: (words: Array<Object>) => Array<Object> = (words) => {
    const newWordList =  words.filter((wordEntry: Word)=> !isExcluded(wordEntry.word, exclusions))
    return newWordList
}

const isExcluded: (word: String, exclusions: List<String>) => Boolean = (word, exclusions) => {
    const matches = exclusions.filter(exclusion => exclusion === word)
    return matches.size !== 0
}


const addWordsByQuote: ActionCreator<Action> = (words: Array<Object>) => {
    return {
        type: keys.ADD_WORDS_BY_QUOTE,
        payload: {
            data: words
        }
    }
}

const addAllWords: ActionCreator<Action> = (words: Array<Object>) => {
    return {
        type: keys.ADD_ALL_WORDS,
        payload: {
            data: words
        }
    }
}

const addFilteredWords: ActionCreator<Action> = (words: Array<Object>) => {
    return {
        type: keys.ADD_FILTERED_WORDS,
        payload: {
            data: words
        }
    }
}


export const setCurrentWord: ActionCreator<any> = (word: String) => {
    return {
        type: keys.SET_CURRENT_WORD,
        payload: {
            data: word
        }
    }
}








