import keys from "../actionTypeKeys";

export interface AddWordsByQuote {
    readonly type: keys.ADD_WORDS_BY_QUOTE;
    readonly payload: {
        data: Object
    }
}

export interface  AddAllWords {
    readonly type: keys.ADD_ALL_WORDS;
    readonly payload: {
        data: Object
    }
}

export interface  AddFilteredWords {
    readonly type: keys.ADD_FILTERED_WORDS;
    readonly payload: {
        data: Object
    }
}

export interface SetCurrentWord {
    readonly type: keys.SET_CURRENT_WORD;
    readonly payload: {
        data: String
    }
}