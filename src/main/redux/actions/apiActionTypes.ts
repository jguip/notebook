import {
    SetApiStatus
} from "./api/apiActionTypes";


type ApiActionTypes =
    SetApiStatus


export default ApiActionTypes;