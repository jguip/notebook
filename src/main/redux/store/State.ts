export default interface State {
    app:{
        apiStatus: String
        processing: String
    }
    content:{
        entries: {
            entries: Array<Object>
            quotes: Array<Object>,
            authors: Array<Object>,
            books: Array<Object>,
            currentQuotes: Array<Object>,
            apiStatus: String
        }
        words: {
            wordsByQuote: Array<Object>
            allWords: Array<Object>
            filteredWords: Array<Object>
            currentWord: String
        }
    }
}
