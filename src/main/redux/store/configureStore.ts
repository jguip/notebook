
import { applyMiddleware, createStore } from "redux";
import thunkMiddleware from "redux-thunk";
import rootReducer from '../reducers'
import State from "./State";

export function configureStore() {
    return createStore<State>(
        rootReducer,
        applyMiddleware(thunkMiddleware)
    );
}

const Store = configureStore();

export default Store
