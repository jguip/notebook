export interface Quote {
    body: any,
    id: String,
    author: {
        authorName? : String,
        authorBio?: String,
        authorId? : String
    }
    book: {
        bookDescription?: String,
        bookId? : String,
        bookTitle? : String
    }
}
