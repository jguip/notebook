import { connect } from 'react-redux';
import  State  from '../../redux/store/State';
import QuoteList from '../QuoteList'


const mapStateToProps = (state: State) => ({
    quotes: state.content.entries.currentQuotes
});

export const SFCQuoteListConnected = connect(mapStateToProps)(QuoteList);