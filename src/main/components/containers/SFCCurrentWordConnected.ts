import { connect } from 'react-redux';
import  State  from '../../redux/store/State';
import CurrentWord from '../CurrentWord'


const mapStateToProps = (state: State) => ({
    word: state.content.words.currentWord
});

export const SFCCurrentWordConnected = connect(mapStateToProps)(CurrentWord);