///put this in a connected word component
import State from "../../redux/store/State";
import {Dispatch} from "redux";
import {WordListItem} from "../WordListItem";
import {connect} from "react-redux";
import {getAndUpdateCurrentQuotes} from "../../redux/actions/entries/entriesActions";
import {setCurrentWord} from "../../redux/actions/words/wordActions";

interface DispatchFromProps {
    onWordClick: () => void;
}


const mapDispatchToProps = (dispatch: Dispatch<State>, ownProps: any) : DispatchFromProps => {
    return {
        onWordClick: () => {
            dispatch(setCurrentWord(ownProps.word.word))
            dispatch(getAndUpdateCurrentQuotes(ownProps.word.quoteIds))
        }
    }
}


export const SFCWordListItemConnected = connect<void, DispatchFromProps, void>(
    null,
    mapDispatchToProps
)(WordListItem);



