import { connect } from 'react-redux';
import  State  from '../../redux/store/State';
import WordList from '../WordsList'


const mapStateToProps = (state: State) => ({
    words: state.content.words.filteredWords

});

export const SFCWordListConnected = connect(mapStateToProps)(WordList);
