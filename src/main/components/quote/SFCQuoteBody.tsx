import * as React from "react"

interface SFCQuoteBodyProps{
    body: any
}
const SFCQuoteBody: React.SFC<SFCQuoteBodyProps> = (props: SFCQuoteBodyProps) => {

    function createBodyHtml() {
        return {__html: props.body};
    }


    return (
        <div dangerouslySetInnerHTML={createBodyHtml()} />
    )
}

export default SFCQuoteBody
