import * as React from 'react'
import {Quote} from "../redux/store/stateTypes";
import SFCQuoteBody from './quote/SFCQuoteBody'


interface QuoteProps {
    quotes: Array<Object>,
}

const QuoteList: React.SFC<QuoteProps> = (props: QuoteProps) => {

    return(
        <div  style={{textAlign: 'left', padding: '100px'}} className="words">
            {props.quotes.map(
                (quote:Quote) =>
                    <div>
                        <SFCQuoteBody body={quote.body} />
                        <div style={{marginBottom: '30px'}}>
                            <span
                                style={{
                                    fontWeight: 700,
                                    fontSize: '14px',
                                    textTransform: 'uppercase',
                                    marginRight: '10px'
                                }}>
                                {quote.author.authorName}</span>
                            <span
                                style={{
                                    fontSize: '12px',
                                    fontWeight: 200,
                                    color: '#ccc',
                                    textTransform: 'italic'
                                }}
                            >
                                {quote.book.bookTitle}
                                </span>
                        </div>
                    </div>

            )}
        </div>

    )
}

export default QuoteList
