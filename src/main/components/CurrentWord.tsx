import * as React from "react"

interface CurrentWordProps{
   word: String
}

const CurrentWord: React.SFC<CurrentWordProps> = (props:CurrentWordProps) => {
    return (
        <span>{props.word}</span>
    )
}

export default CurrentWord
