import * as React from "react"

interface WordListItemProps{
    word: {
        quoteIds: Array<String>
        word: String
    }
    onWordClick: () => void
}

export const WordListItem: React.SFC<WordListItemProps> = (props: WordListItemProps) => {
    return (
            <span
                style={{
                    display: 'inline-block',
                    marginRight: '10px',
                    cursor: 'pointer',
                    fontSize: '12px',
                    color: '#ccc'
                }}
                onClick={props.onWordClick}>
                {props.word.word}
            </span>
        )
}