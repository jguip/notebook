import * as React from "react"
import {SFCWordListItemConnected} from './containers/SFCWordListItemConnected';

interface Word{
    word: String,
    quoteIds: Array<String>
}

interface WordsProps {
    words: Array<Object>,
}

const WordsList: React.SFC<WordsProps> = (props: WordsProps) => {
    return(
        <div  style={{textAlign: 'left', padding: '100px', height: '600px', overflowY: 'scroll' }} className="words">
            <div>Add Highlight</div>
            {props.words.map(
                (word:Word) => <SFCWordListItemConnected word={word}/>
            )}
        </div>

    )
}

export default WordsList