import * as React from 'react';
import './App.css';
import {loadContentfulEntries} from "./main/api";
import Store from './main/redux/store/configureStore'
import {SFCWordListConnected} from './main/components/containers/SFCWordsListConnected'
import {SFCQuoteListConnected} from "./main/components/containers/SFCQuoteListConnected";
import {SFCCurrentWordConnected} from "./main/components/containers/SFCCurrentWordConnected";

class App extends React.Component {

   componentDidMount(){
        loadContentfulEntries()
        Store.subscribe(this.render)
   }

  render() {
  
    return (

      <div className="App">
          <div style={{width: '50%', float: 'left'}}>
              <SFCWordListConnected />

          </div>
          <div style={{width: '50%', float: 'left'}} >
              <SFCCurrentWordConnected/>
              <SFCQuoteListConnected/>
          </div>
      </div>

    );
  }
}

export default App;
